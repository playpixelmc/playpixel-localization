<div align="center">

 # Woodix :: Translations
 
 
 ***
Данный репозиторий используется для мульти-язяковой системы с проекта **Woodix Project (mc.woodix.me)**
</div>

---

### Обратная связь

---
Если у Вас есть какие-то предложения или желание в помощи по переводу, 
то можете обратиться к нам через указанные контакты:


* **[Discord](https://woodix.me/discord)**
* **[Telegram](https://t.me/woodixme)**
